from AthenaCommon.CfgGetter import addAlgorithm
addAlgorithm("CscOverlay.CscOverlayConfig.getCscOverlay", "CscOverlay")
addAlgorithm("CscOverlay.CscOverlayConfig.getCscOverlayDigitToRDO", "CscOverlayDigitToRDO")
